# Build bees first
FROM docker.io/rust:alpine AS BUILDER

WORKDIR /root
RUN apk add --no-cache musl-dev
RUN cargo install espflash

# This creates the actual finished container
FROM docker.io/alpine:latest

ENV NAME=espflash ARCH=x86_64
LABEL   name="$NAME" \
        architecture="$ARCH" \
        run="podman run -v /dev:/dev -v $PWD:/project:z -i --security-opt label=disable IMAGE ..." \
        summary="Flash binaries to an esp MCU." \
        maintainer="Andreas Hartmann <hartan@7x.de>" \
        url="https://gitlab.com/c8160/embedded-rust/espflash"

# Move bees to a new container
COPY --from=BUILDER /usr/local/cargo/bin/espflash /usr/bin/espflash

# Copy documentation into container
COPY help.md /

VOLUME /project
WORKDIR /project

ENTRYPOINT [ "/usr/bin/espflash" ]
