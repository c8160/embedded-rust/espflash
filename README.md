# espflash

espflash based on Alpine Linux.

> This image is tested for use with **podman** on **Linux** hosts with **x86_64** arch


## What is espflash?

espflash is a serial flasher utility for Espressif SoCs and modules based on
esptool.py. Currently supports the ESP32, ESP32-C3, ESP32-S2, ESP32-S3, and
ESP8266.

See [the project website](https://github.com/esp-rs/espflash).


## How to use this image

### As a replacement for "regular" espflash

```bash
$ podman run --rm -it --security-opt label=disable -v /dev:/dev -v "$PWD:/project:z" registry.gitlab.com/c8160/embedded-rust/espflash --help
```

This will run espflash with `--help` as argument.

We add the `/dev` directory from the host so the container can access the
serial device for any attached ESP chip. On **SELinux**-enabled systems we
additionally pass the `--security-opt label=disable` to podman because by
default containers aren't allowed to access devices on the host system.

Note that the container will have the same access permissions as the user that
executes it. This means that if your user isn't allowed to read/write to the
serial device, the container isn't either. To circumvent this you can:

- Add a udev rule (**preferred**), or
- `chmod 666 /dev/ttyUSB*`, or
- `chown $(id -un):$(id -gn) /dev/ttyUSB*`



## Getting help

If you feel that something isn't working as expected, open an issue in the
Gitlab project for this container and describe what issue you are facing.

