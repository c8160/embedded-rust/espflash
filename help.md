espflash image for running espflash from a container.

Packages the [espflash utility](https://crates.io/crates/espflash).

Volumes:
PWD: Mapped to /project in the container, used to exchange files with the host.
  Without this, the flash utility cannot access the binary file to flash. Note
  that if you mount your $PWD, the binary to flash must be accessible from
  within or below the $PWD. Relative paths starting with ".." aren't allowed.
/dev: Mapped to /dev in the container, required to communicate with the ESP MCU
  to flash the software to.

Documentation:
For the underlying espflash project, see https://github.com/esp-rs/espflash
For this container, see https://gitlab.com/c8160/embedded-rust/espflash

Requirements:
Needs an ESP device to flash the firmware to. Most important, this device must
be read/writable by the user executing the container. You can either achieve
this by `chown`ing or `chmod`ing the respective entry in `/dev/ttyUSB*`. Or,
for a more permanent solution, create a udev rule to do this automatically for
you.
